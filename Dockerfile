FROM debian:11-slim

LABEL description="Toolkit for RSS to Markdown example"

LABEL maintainer="Alec Clews <alecclews@gmail.com>" 

RUN apt-get update && apt-get -y install curl libxml2-utils
