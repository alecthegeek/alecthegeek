# Stuff about Alec the Geek

* Developer relations advocate and computer consultant

* I live and work on Bunurong Land, Melbourne, Australia

* I run Linux in many places. Debian or Apline in Docker or Google Cloud Run , Raspian on Raspberry Pi's, and Debian in ChromeOS.

* As a devrel I can program in many different computer languages, but I'm not an expert in single one

* I like to explain and teach things

* I often speak at meetups and conferences. Ping me if you want me to speak to your group

* You can find my links on the right

## My last five [blog](http://alecthegeek.tech/blog/) posts

  <!-- BLOG_FEED_START -->
  | Title | Description | Date |
  |-------|-------------|------|
| [Manage Git and Hook Scripts the easy way](https://alecthegeek.tech/blog/2024/08/manage-git-and-hook-scripts-the-easy-way/) | Use the hooksPath config setting to manage hook scripts under version control | 28 Aug 2024 |
| [Spell Check with Vale](https://alecthegeek.tech/blog/2024/05/spell-check-with-vale/) | Implement better spellchecking with Vale custom spelling rules | 27 May 2024 |
| [Add Vale to Your Project](https://alecthegeek.tech/blog/2024/05/add-vale-to-your-project/) | Use Vale text linting to improve your technical writing and automate your style guide | 09 May 2024 |
| [Use GitHub Actions to update your profile page](https://alecthegeek.tech/blog/2023/10/use-github-actions-to-update-your-profile-page/) | Update your GitHub profile README with content from an RSS feed the hard way | 07 Oct 2023 |
| [Commit new content in a GitLab CI/CD pipeline the easy way](https://alecthegeek.tech/blog/2023/03/commit-new-content-in-a-gitlab-ci/cd-pipeline-the-easy-way/) | It’s not obvious how to commit new content back to your GitLab project repo. Try this pattern | 21 Mar 2023 |
  <!-- BLOG_FEED_END -->




